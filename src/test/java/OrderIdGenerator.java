import com.hty.baseframe.idgenerator.IdGenerator;
import com.hty.baseframe.idgenerator.exception.IdStructureException;
import com.hty.baseframe.idgenerator.structure.IdStructure;
import com.hty.baseframe.idgenerator.structure.impl.*;
import com.hty.baseframe.idgenerator.util.TimeStructureTypes;

public class OrderIdGenerator extends IdGenerator {

    public OrderIdGenerator(String generatorName) throws IdStructureException {
        IdStructure ts = new TimeStructure(
                        TimeStructureTypes.TIME_YEAR +
                        TimeStructureTypes.TIME_MONTH +
                        TimeStructureTypes.TIME_DATE +
                        TimeStructureTypes.TIME_HOUR +
                        TimeStructureTypes.TIME_MINUTE +
                        TimeStructureTypes.TIME_SECOND
                        );
        IdStructure dbs = new DBRouteStructure();
        IdStructure tbs = new TableRouteStructure();
        IdStructure mid = new MachineStructure("01");
        IdStructure inc = new IncrementStructure(3, 1);
        this.addIdStructure(ts);
        this.addIdStructure(dbs);
        this.addIdStructure(tbs);
        this.addIdStructure(mid);
        this.addIdStructure(inc);
        this.publish();
    }


}
