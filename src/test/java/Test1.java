import com.hty.baseframe.idgenerator.IdGenerator;
import com.hty.baseframe.idgenerator.IdGeneratorFactory;
import com.hty.baseframe.idgenerator.exception.IdStructureException;
import org.junit.Test;

public class Test1 {

    @Test
    public void test() throws IdStructureException {
        new OrderIdGenerator("OrderIdGenerator");
        IdGenerator generator = IdGeneratorFactory.getIdGenerator(OrderIdGenerator.class);
        for (int i = 0; i < 1000; i++) {
            System.out.println(generator.generate());
        }
    }
}
