package com.hty.baseframe.idgenerator.exception;

public class IdStructureException extends Exception {

	private static final long serialVersionUID = -4339600168739653135L;

	public IdStructureException() {}

	public IdStructureException(Exception e) { super(e); }

	public IdStructureException(String message) {
		super(message);
	}
	
}
