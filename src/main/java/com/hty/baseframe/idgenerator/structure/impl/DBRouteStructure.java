package com.hty.baseframe.idgenerator.structure.impl;

import com.hty.baseframe.idgenerator.structure.IdStructure;

import java.util.Map;
/**
 * 产生ID部分的数据库路由ID信息，根据此ID可以路由到此ID表示的记录所在数据库。
 * 传入的参数需要一个包含数据库ID的键值对："DB_NO"
 * @author Hetianyi 2017/12/30
 * @version 1.0
 */
public class DBRouteStructure extends IdStructure {
	
	private static final long serialVersionUID = 5105435841957509964L;
	
	private String fetchKey = "DB_NO";

	@Override
	public Object generate(Map<String, Object> params) {
		if(null != params) {
			return params.get(fetchKey);
		}
		return "";
	}
}
