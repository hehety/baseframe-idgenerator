package com.hty.baseframe.idgenerator.structure.impl;

import com.hty.baseframe.idgenerator.exception.IdStructureException;
import com.hty.baseframe.idgenerator.structure.IdStructure;
import com.hty.baseframe.idgenerator.util.IdUtils;
import com.hty.baseframe.idgenerator.util.TimeStructureTypes;

import java.util.Calendar;
import java.util.Map;

/**
 * 产生ID部分的时间序列
 * @author Hetianyi
 * @author Hetianyi 2017/12/30
 * @version 1.0
 */
public final class TimeStructure extends IdStructure {
	
	private static final long serialVersionUID = 8429868770685144129L;
	/**
	 * ID结构类型
	 */
	private int structureType;
	/**
	 * 当没有指定StructureType时，默认使用全格式的时间格式：StructureType.TIME
	 * @throws IdStructureException
	 */
	public TimeStructure() {
		this.structureType = TimeStructureTypes.TIME;
	}
	
	public TimeStructure(int structureType) {
		this.structureType = structureType;
	}
	
	public String generate(Map<String, Object> params) {
		Calendar can = IdUtils.getCalendar();
		return parseTimeStructure(can);
	}

	/**
	 * 根据日期生成日期串
	 * @param can
	 * @return
	 */
	private String parseTimeStructure(Calendar can) {
		StringBuilder sb = new StringBuilder();
		if(this.structureType == TimeStructureTypes.TIME) {
			sb.append(getYear(can)).append(getMonth(can)).append(getDate(can))
			  .append(getHour(can)).append(getMinute(can)).append(getSecond(can)).append(getMilliSecond(can));
			return sb.toString();
		}
		if(this.structureType % 2 == TimeStructureTypes.TIME_YEAR) {
			sb.append(getYear(can));
		}
		if((this.structureType % TimeStructureTypes.TIME_DATE) / TimeStructureTypes.TIME_MONTH == 1) {
			sb.append(getMonth(can));
		}
		if((this.structureType % TimeStructureTypes.TIME_HOUR) / TimeStructureTypes.TIME_DATE == 1) {
			sb.append(getDate(can));
		}
		if((this.structureType % TimeStructureTypes.TIME_MINUTE) / TimeStructureTypes.TIME_HOUR == 1) {
			sb.append(getHour(can));
		}
		if((this.structureType % TimeStructureTypes.TIME_SECOND) / TimeStructureTypes.TIME_MINUTE == 1) {
			sb.append(getMinute(can));
		}
		if((this.structureType % TimeStructureTypes.TIME_MILLISECOND) / TimeStructureTypes.TIME_SECOND == 1) {
			sb.append(getSecond(can));
		}
		if((this.structureType / TimeStructureTypes.TIME_MILLISECOND) == 1) {
			sb.append(getMilliSecond(can));
		}
		try {
			return sb.toString();
		} catch (Exception e) {
			return sb.toString();
		} finally {
			sb.delete(0, sb.length());
			sb = null;
		}
	}
	
	private String getYear (Calendar can) {
		return String.valueOf(can.get(Calendar.YEAR));
	}
	private String getMonth (Calendar can) {
		return IdUtils.timeFixZero(can.get(Calendar.MONTH) + 1, 2);
	}
	private String getDate (Calendar can) {
		return IdUtils.timeFixZero(can.get(Calendar.DAY_OF_MONTH), 2);
	}
	private String getHour (Calendar can) {
		return IdUtils.timeFixZero(can.get(Calendar.HOUR_OF_DAY), 2);
	}
	private String getMinute (Calendar can) {
		return IdUtils.timeFixZero(can.get(Calendar.MINUTE), 2);
	}
	private String getSecond (Calendar can) {
		return IdUtils.timeFixZero(can.get(Calendar.SECOND), 2);
	}
	private String getMilliSecond (Calendar can) {
		return IdUtils.timeFixZero(can.get(Calendar.MILLISECOND), 3);
	}
	public int getStructureType() {
		return structureType;
	}
	public void setStructureType(int structureType) {
		this.structureType = structureType;
	}
}
