package com.hty.baseframe.idgenerator.structure.impl;

import com.hty.baseframe.idgenerator.structure.IdStructure;

import java.util.Map;

/**
 * ID类型结构体，可以区分不同对象的ID
 * @author Hetianyi 2017/12/30
 * @version 1.0
 */
public class IdTypeStructure extends IdStructure {

    private String idType;

    public IdTypeStructure(String idType) {
        this.idType = idType;
    }

    @Override
    public Object generate(Map<String, Object> params) {
        return null == idType ? "" : idType.trim();
    }
}
