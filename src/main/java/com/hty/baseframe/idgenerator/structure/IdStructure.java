package com.hty.baseframe.idgenerator.structure;

import java.util.Map;

/**
 * Id结构体父类
 * @author Hetianyi 2017/12/30
 * @version 1.0
 */
public abstract class IdStructure {
    /**
     * 结构体输出Id结构
     * @param params 动态结构体的参数
     * @return
     */
    public abstract Object generate(Map<String, Object> params);
}
