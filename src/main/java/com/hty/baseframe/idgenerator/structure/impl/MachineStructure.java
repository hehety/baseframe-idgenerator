package com.hty.baseframe.idgenerator.structure.impl;

import com.hty.baseframe.idgenerator.structure.IdStructure;

import java.util.Map;

/**
 * ID生成器所在的机器序号，根据需要设置为1位即可。
 * 如果只有一台机器，则不需要改结构
 * @author Hetianyi 2017/12/30
 * @version 1.0
 */
public class MachineStructure extends IdStructure {

	/** 机器序号 */
	private String machineId;
	
	public MachineStructure(String machineId) {
		this.machineId = machineId;
	}
	
	@Override
	public String generate(Map<String, Object> params) {
		return machineId;
	}

	public String getMachineId() {
		return machineId;
	}
}
