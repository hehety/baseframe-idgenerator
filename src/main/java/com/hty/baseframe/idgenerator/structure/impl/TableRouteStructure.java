package com.hty.baseframe.idgenerator.structure.impl;

import com.hty.baseframe.idgenerator.structure.IdStructure;

import java.util.Map;
/**
 * 产生ID部分的分表路由ID信息，根据此ID可以路由到此ID表示的记录分表ID。
 * 传入的参数需要一个包含分表ID的键值对："TB_NO"
 * @author Hetianyi 2017/12/30
 * @version 1.0
 */
public class TableRouteStructure extends IdStructure {
	
	private static final long serialVersionUID = 6556784025675633039L;

	private String fetchKey = "TB_NO";
	
	@Override
	public Object generate(Map<String, Object> params) {
		if(null != params) {
			return params.get(fetchKey);
		}
		return "";
	}

}
