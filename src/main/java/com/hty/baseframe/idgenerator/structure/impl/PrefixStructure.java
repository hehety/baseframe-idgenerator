package com.hty.baseframe.idgenerator.structure.impl;

import com.hty.baseframe.idgenerator.structure.IdStructure;

import java.util.Map;

/**
 * 产生ID部分的前缀
 * @author Hetianyi 2017/12/30
 * @version 1.0
 */
public final class PrefixStructure extends IdStructure {
	
	private static final long serialVersionUID = 6249712488388308727L;
	/** 前缀信息 */
	private String prefix;
	
	public PrefixStructure(String prefix) {
		this.prefix = null == prefix ? "" : prefix.trim();
	}
	
	@Override
	public String generate(Map<String, Object> params) {
		return prefix;
	}
	
	public String getPrefix() {
		return prefix;
	}
}
