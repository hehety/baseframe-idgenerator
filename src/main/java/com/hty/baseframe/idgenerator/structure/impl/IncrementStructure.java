package com.hty.baseframe.idgenerator.structure.impl;

import com.hty.baseframe.idgenerator.structure.IdStructure;
import com.hty.baseframe.idgenerator.util.IdUtils;

import java.math.BigDecimal;
import java.util.Map;

/**
 * ID部分的自增序列，通常时间部分最长达到毫秒级，<br>
 * 当一毫秒内有多个ID生成时，需要在此毫秒内设置一个自增序列来确保ID的唯一性。<br>
 * 同时，当时间部分不设置到毫秒级而是天级别，此时更需要自增序列。<br>
 * 自增序列的部分的长度和事件部分精确度成简单的反比关系。<br>
 * 例如，时间部分设置到毫秒级，此时自增部分长度可以设置成3位；<br>
 * 当时间部分设置到天级，此时自增部分长度可以根据业务量设置为7+位。
 * @author Hetianyi 2017/12/30
 * @version 1.0
 */
public class IncrementStructure extends IdStructure {
	
	private static final long serialVersionUID = -1511753317355836755L;
	/** 自增序号 */
	private int width = 3;
	/**  */
	private long exceedValue;
	/** 自增步长 */
	private int increment = 1;
	/** 当前自增值 */
	private long value = 0;
	/**
	 * 自增结构
	 * @param width 长度
	 * @param increment 自增步长
	 */
	public IncrementStructure(int width, int increment) {
		this.width = width;
		this.increment = increment;
		this.exceedValue = Long.valueOf(String.valueOf(new BigDecimal(Math.pow(10, this.width))));
	}
	
	@Override
	public String generate(Map<String, Object> params) {
		return increValue();
	}
	/**
	 * 自增
	 * @return
	 */
	private synchronized String increValue() {
		value += increment;
		if(value >= exceedValue) {
			value = 1;
		}
		return IdUtils.fixLength(value, width);
	}

	public int getWidth() {
		return width;
	}

	public int getIncrement() {
		return increment;
	}
}
