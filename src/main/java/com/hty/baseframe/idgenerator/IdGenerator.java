package com.hty.baseframe.idgenerator;

import com.hty.baseframe.idgenerator.structure.IdStructure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * IdGenerator的超类，集合了一系列IdStructure，按照IdStructure的加入顺序拼凑结构体。
 * @author Hetianyi 2017/12/30
 * @version 1.0
 */
public class IdGenerator {

    private static final Logger logger = LoggerFactory.getLogger(IdGenerator.class);

    /** 自定义结构体列表 */
    private List<IdStructure> structures = new ArrayList<IdStructure>(5);
    /**
     * 为该ID生成器添加结构体
     * @param idStructure
     */
    protected void addIdStructure(IdStructure idStructure) {
        this.structures.add(idStructure);
    }
    /**
     * 发布生成器
     */
    protected void publish() {
        IdGeneratorFactory.publish(this);
    }
    /**
     * 生成ID（没有参数）
     * @return
     */
    public Object generate() {
        return generate(null);
    }
    /**
     * 动态生成ID（有参数）
     * @param params
     * @return
     */
    public Object generate(Map<String, Object> params) {
        StringBuilder sb = new StringBuilder();
        StringBuilder debugInfo = new StringBuilder();
        for(IdStructure s : structures) {
            String part = (String) s.generate(params);
            sb.append(part);
            debugInfo.append(part).append("(").append(s.getClass().getSimpleName()).append(")|");
        }
        logger.debug("Generate Id › {}", debugInfo.toString().substring(0, debugInfo.toString().length() - 1));
        return sb.toString();
    }
}
