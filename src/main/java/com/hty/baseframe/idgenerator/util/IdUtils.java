package com.hty.baseframe.idgenerator.util;

import java.util.Calendar;

public class IdUtils {
	/**
	 * 获取系统当前日期
	 * @return Calendar
	 */
	public static Calendar getCalendar() {
		return Calendar.getInstance();
	}
	/**
	 * 处理时间字符拼接时缺少0
	 * @param input 时间：日时分秒的正整数值
	 * @param width 最大补齐宽度，如：日时分秒为2，毫秒为3
	 * @return 补齐0的时间
	 */
	public static String timeFixZero(long input, int width) {
		if (width == 2) {
			return input < 10 ? ("0" + input) : String.valueOf(input);
		} else {
			return input < 10 ? ("00" + input) : (input < 100 ? ("0" + input) : String.valueOf(input));
		}
	}
	/**
	 * 将数字转换为固定长度的字符串，不足长度的在前补0
	 * @param input
	 * @param width
	 * @return
	 */
	public static String fixLength(long input, int width) {
		StringBuilder sb = new StringBuilder(String.valueOf(input));
		int len = sb.length();
		if(len < width) {
			for(int i = len; i < width; i++) {
				sb.insert(0, '0');
			}
		}
		return sb.toString();
	}
}
