package com.hty.baseframe.idgenerator.util;

public class TimeStructureTypes {
	/**  默认时间格式: yyyyMMddhhmmssms */
	public static final int TIME 				= 0;
	/**  时间格式: yyyy */
	public static final int TIME_YEAR 			= 1;
	/**  时间格式: MM */
	public static final int TIME_MONTH 			= 10;
	/**  时间格式: dd */
	public static final int TIME_DATE 			= 100;
	/**  时间格式: hh */
	public static final int TIME_HOUR 			= 1000;
	/**  时间格式: mm */
	public static final int TIME_MINUTE 		= 10000;
	/**  时间格式: ss */
	public static final int TIME_SECOND 		= 100000;
	/**  时间格式: ms */
	public static final int TIME_MILLISECOND 	= 1000000;
}
