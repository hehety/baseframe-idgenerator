package com.hty.baseframe.idgenerator;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * ID生成器工厂类
 * @author Hetianyi 2017/12/30
 * @version 1.0
 */
public class IdGeneratorFactory {
    /** Id生成器容器 */
    private static final Map<Class<?>, IdGenerator> registeredIdGenerators =
            new HashMap<Class<?>, IdGenerator>();
    /**
     * 发布ID生成器
     * @param generator
     */
    static void publish(IdGenerator generator) {
        if(!registeredIdGenerators.containsKey(generator.getClass())) {
            registeredIdGenerators.put(generator.getClass(), generator);
        }
    }
    /**
     * 获取一个Id生成器
     * @param clazz
     * @return
     */
    public static IdGenerator getIdGenerator(Class<?> clazz) {
        return registeredIdGenerators.get(clazz);
    }
    /**
     * 获取所有的Id生成器
     * @return
     */
    public static Collection<IdGenerator> getAllIdGenerator() {
        return registeredIdGenerators.values();
    }

}
